export class Account {
    id:number;
    accountHolderName:string;
    branchname:string;
    initialBalance:number;
    accountNumber:number;
}
