export class Transactions {
    "id": number;
    "tId":number;
    "userId": number;
    "ammount": number;
    "toName":string;
    "toAccountNumber":number;
    "date": Date;
    "balance": number;
}
