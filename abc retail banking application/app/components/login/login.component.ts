import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AccountService } from 'src/app/services/account.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  constructor(private userService: UserService, private httpclient: HttpClient, private formBuilder: FormBuilder, private router: Router) { }

  public getdata: any = [];
  count: number;

  getAllDetails() {
    this.userService.getUserdetails().subscribe((data) => {
      this.getdata = data;
      console.log("getdata", this.getdata);
    })
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      customerId: ['', [Validators.required, Validators.minLength(6)]],
      password: ['', [Validators.required]],
    });
    this.getAllDetails()
  }
  get f() { return this.registerForm.controls; }
  onSubmit() {
    console.log("customerId: " + this.registerForm.controls.customerId.value + 'password: ' + this.registerForm.controls.password.value);
    for (var i = 0; i < this.getdata.length; i++) {
      if ((this.registerForm.controls.customerId.value == this.getdata[i].customerId) &&
        (this.registerForm.controls.password.value == this.getdata[i].password)) {
        if (this.getdata[i].type === "user") {
          this.count = 1;
          break;
        }
        else {
          this.count = 2;
          break;
        }
      }
      else {
        this.count = 0;
      }
    }
    console.log(this.count);
    if (this.count == 1)
      this.router.navigate(['/transaction']);
    else if (this.count == 2)
      this.router.navigate(['/create-account', this.registerForm.controls.customerId.value]);
    else
      alert("You Have Entered Invalid customerId or Password");
  }


  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }

}
