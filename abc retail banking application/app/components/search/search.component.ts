import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {


  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getTransactions();
  }
  //transacti=new Transactions();
  trasactionlist: any = null;
  items = ["accountNumber", "balance", "date"];
  selectedItem = '';

  userId = '';
  accountNumber = '';
  startDate = ''
  endDate = ''

  getBydate(startDate, endDate) {

    console.log(startDate)
    var std = new Date(startDate);
    var etd = new Date(endDate);

    this.trasactionlist = [];

    this.userService.getBydate(startDate, endDate).subscribe(res => {
      console.log(res);
      res.forEach(t => { this.trasactionlist.push(t); });

    })





  }


  getlistbyaccountnumber(accountNumber) {
    this.trasactionlist = [];
    this.userService.getTransactionsByAccountNumber(accountNumber).subscribe(res => {
      res.forEach(t => { this.trasactionlist.push(t); });


    });

  }

  getlist(userId) {
    console.log("--------------------" + userId)

    this.trasactionlist = [];
    this.userService.getTransactionsById(userId).subscribe(res => {
      res.forEach(t => { this.trasactionlist.push(t); });


    });
  }






  selected() {


  }


  getTransactions() {
    this.trasactionlist = [];
    this.userService.getTrans().subscribe(res => {

      res.forEach((t, index) => {

        this.trasactionlist.push(t);
        console.log(JSON.stringify(this.trasactionlist))

      });
    });



  }



}
