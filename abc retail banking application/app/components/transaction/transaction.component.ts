import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { Transactions } from 'src/app/models/transactions';
import { Account } from 'src/app/models/account';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {

  fromAccountNumber;
  toAccountNumber;
  ammount;
  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }
  transaction(fromAccountNumber, toAccountNumber, ammount) {
    let sender = new User();
    let receiver = new User();
    console.log(ammount)


    this.userService.getByAccountNumber(fromAccountNumber).subscribe(res => {
      sender = res[1]
      console.log(sender)
      console.log(sender.account)
      if (sender.account.initialBalance > ammount && ammount > 0) {

        sender.account.initialBalance = sender.account.initialBalance - ammount
        console.log(sender.account.initialBalance)
        console.log(sender.id);
        this.userService.updateUser(sender.id, sender).subscribe(data=>{console.log(data)})
        console.log(sender);

        this.userService.getByAccountNumber(toAccountNumber).subscribe(res => {
          receiver = res[2]
          console.log(receiver.account.accountNumber)
          receiver.account.initialBalance = receiver.account.initialBalance + ammount
          console.log(receiver.account.initialBalance)
          this.userService.updateUser(receiver.id, receiver).subscribe()
          console.log(receiver);
          this.userService.getTrans().subscribe(result => {
            console.log(result.length)



            let trans = new Transactions();
            trans.ammount = ammount;
            trans.balance = sender.account.initialBalance;
            trans.date = new Date();
            trans.userId = sender.id;
            trans.toAccountNumber = receiver.account.accountNumber;
            trans.toName = receiver.customerName;

            trans.id = result.length + 1;
            trans.tId = result.length + 1;

            this.userService.saveTransactionsDetails(trans).subscribe(res => {
              console.log(JSON.stringify(res));

            });

          });
        });


        alert("transaction is successfull");




      } else {

        alert("please enter correct details")
      }


    });


  }

}
