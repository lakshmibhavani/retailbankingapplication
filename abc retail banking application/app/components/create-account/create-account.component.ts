import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Account } from 'src/app/models/account';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit {

  account:Account=new Account();
  user:User=new User();
  constructor(private router:Router,private userService:UserService) { }

  editUser() {
    
    this.userService.getUser(this.account.id).subscribe(data => {
      
      this.user.account=this.account;
      this.user=data;
    })
  }
  ngOnInit(): void {
    this.editUser();
  }
  onSubmit(userForm){
    if(userForm.form.valid){
   this.updateUser();
    }
  }
  updateUser() {
    console.log("into update");
    this.userService.updateUser(this.account.id,this.user).subscribe(data => { console.log(data); }, error => console.log(error));
  }

}




