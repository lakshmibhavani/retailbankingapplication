import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { CreateAccountComponent } from './components/create-account/create-account.component';
import { TransactionComponent } from './components/transaction/transaction.component';
import { SearchComponent } from './components/search/search.component';

const routes: Routes = [{path:'login',component:LoginComponent},
{ path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'create-account/:id', component: CreateAccountComponent },
  { path: 'transaction', component: TransactionComponent },
  { path: 'search', component: SearchComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
